#!/bin/bash

echo '### criando localstack'

echo '### Criando SQS -> poc_student'
awslocal sqs create-queue --queue-name poc_student
awslocal sqs send-message --queue-url http://localhost:4566/000000000000/poc_student --message-body '{"id":"ff0ab862-3ba5-11ee-93a3-8f44169e66a8","name":"Student POC 1", "birth_day" : "2001-01-01"}'
awslocal sqs send-message --queue-url http://localhost:4566/000000000000/poc_student --message-body '{"id":"be0d7eee-45cd-11ee-bf3b-6f7591e3b16c","name":"Student POC 2", "birth_day" : "2011-11-11"}'


echo '### finalizando localstack'
echo "";
echo 'acesse ->  https://app.localstack.cloud.'

exit 0