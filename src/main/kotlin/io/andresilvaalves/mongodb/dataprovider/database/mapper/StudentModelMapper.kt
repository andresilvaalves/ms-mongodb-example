package io.andresilvaalves.mongodb.dataprovider.database.mapper

import io.andresilvaalves.mongodb.core.domain.StudentDomain
import io.andresilvaalves.mongodb.dataprovider.database.model.StudentModel
import org.mapstruct.Mapper
import org.mapstruct.MappingConstants
import org.mapstruct.MappingTarget

@Mapper(componentModel = MappingConstants.ComponentModel.SPRING)
interface StudentModelMapper {

    fun toModel(studentDomain: StudentDomain): StudentModel
    fun toDomain(studentModel: StudentModel): StudentDomain
    fun update(@MappingTarget studentModel: StudentModel, studentDomain: StudentDomain)
}