package io.andresilvaalves.mongodb.core.usecase

import io.andresilvaalves.mongodb.core.domain.StudentDomain
import io.andresilvaalves.mongodb.core.gateway.StudentGateway
import org.springframework.data.domain.Page
import org.springframework.stereotype.Service
import java.util.UUID

@Service
class StudentUseCase(
    private val studentGateway: StudentGateway,
) {

    suspend fun save(domain: StudentDomain): StudentDomain {
        return studentGateway.save(domain)
    }

    suspend fun update(uuid: UUID, domain: StudentDomain): StudentDomain {
        return studentGateway.update(uuid, domain)
    }

    suspend fun findAll(active: Boolean, page: Int, pageSize: Int): Page<StudentDomain> {
        return studentGateway.findAll(active, page, pageSize)
    }

    suspend fun findByID(uuid: UUID): StudentDomain {
        return studentGateway.findById(uuid)
    }

    suspend fun findByName(name: String): List<StudentDomain> {
        return studentGateway.findByName(name)
    }

    suspend fun delete(uuid: UUID) {
        studentGateway.delete(uuid)
    }

}
