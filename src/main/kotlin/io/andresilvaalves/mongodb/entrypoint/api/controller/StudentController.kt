package io.andresilvaalves.mongodb.entrypoint.api.controller

import io.andresilvaalves.mongodb.core.usecase.StudentUseCase
import io.andresilvaalves.mongodb.entrypoint.api.mapper.StudentControllerMapper
import io.andresilvaalves.mongodb.entrypoint.api.payload.StudentCreateRequest
import io.andresilvaalves.mongodb.entrypoint.api.payload.StudentRequest
import io.andresilvaalves.mongodb.entrypoint.api.payload.StudentResponse
import org.springframework.data.domain.Page
import org.springframework.http.HttpStatus
import org.springframework.web.bind.annotation.DeleteMapping
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.PathVariable
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.PutMapping
import org.springframework.web.bind.annotation.RequestBody
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RequestParam
import org.springframework.web.bind.annotation.ResponseStatus
import org.springframework.web.bind.annotation.RestController
import java.util.UUID

@RestController
@RequestMapping("/v1/students")
class StudentController(
    private val studentUseCase: StudentUseCase,
    private val studentControllerMapper: StudentControllerMapper,
) {

    @GetMapping
    @ResponseStatus(HttpStatus.OK)
    suspend fun findAll(
        @RequestParam(value = "active", defaultValue = "true") active: Boolean,
        @RequestParam(name = "page", defaultValue = "0") page: Int,
        @RequestParam(name = "pageSize", defaultValue = "10") pageSize: Int,
    ): Page<StudentResponse> {
        return studentUseCase.findAll(active, page, pageSize).map(studentControllerMapper::toResponse)
    }

    @GetMapping("{uuid}")
    @ResponseStatus(HttpStatus.OK)
    suspend fun findByID(@PathVariable uuid: UUID): StudentResponse {
        return studentUseCase.findByID(uuid).let(studentControllerMapper::toResponse)
    }

    @GetMapping("{name}/name")
    @ResponseStatus(HttpStatus.OK)
    suspend fun findByName(@PathVariable name: String): List<StudentResponse> {
        return studentUseCase.findByName(name).map(studentControllerMapper::toResponse)
    }

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    suspend fun save(@RequestBody request: StudentCreateRequest): StudentResponse {
        return studentUseCase.save(studentControllerMapper.toDomain(request))
            .let { studentControllerMapper.toResponse(it) }
    }

    @PutMapping("{uuid}")
    @ResponseStatus(HttpStatus.OK)
    suspend fun update(@PathVariable uuid: UUID, @RequestBody request: StudentRequest): StudentResponse {
        return studentUseCase.update(uuid, studentControllerMapper.toDomain(request))
            .let(studentControllerMapper::toResponse)
    }

    @DeleteMapping("{uuid}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    suspend fun delete(@PathVariable uuid: UUID) = studentUseCase.delete(uuid)
}
