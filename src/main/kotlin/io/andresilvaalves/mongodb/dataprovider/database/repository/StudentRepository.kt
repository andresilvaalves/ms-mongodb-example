package io.andresilvaalves.mongodb.dataprovider.database.repository

import io.andresilvaalves.mongodb.dataprovider.database.model.StudentModel
import org.springframework.data.mongodb.repository.MongoRepository
import java.util.UUID

interface StudentRepository : MongoRepository<StudentModel, UUID> {

    fun findByEmail(email: String) : StudentModel
    fun findByNameLikeIgnoreCase(name: String) : List<StudentModel>
}