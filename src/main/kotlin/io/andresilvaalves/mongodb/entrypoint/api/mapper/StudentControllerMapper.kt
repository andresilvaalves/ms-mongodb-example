package io.andresilvaalves.mongodb.entrypoint.api.mapper

import io.andresilvaalves.mongodb.core.domain.StudentDomain
import io.andresilvaalves.mongodb.entrypoint.api.payload.StudentCreateRequest
import io.andresilvaalves.mongodb.entrypoint.api.payload.StudentRequest
import io.andresilvaalves.mongodb.entrypoint.api.payload.StudentResponse
import org.mapstruct.Mapper
import org.mapstruct.MappingConstants.ComponentModel
import org.mapstruct.ReportingPolicy

@Mapper(componentModel = ComponentModel.SPRING, unmappedSourcePolicy = ReportingPolicy.IGNORE, unmappedTargetPolicy = ReportingPolicy.IGNORE)
interface StudentControllerMapper {

    fun toDomain(studentCreateRequest: StudentCreateRequest): StudentDomain
    fun toDomain(studentRequest: StudentRequest): StudentDomain
    fun toResponse(studentDomain: StudentDomain): StudentResponse
}
