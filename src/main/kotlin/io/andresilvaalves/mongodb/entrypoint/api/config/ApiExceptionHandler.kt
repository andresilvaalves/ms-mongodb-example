package io.andresilvaalves.mongodb.entrypoint.api.config

import org.apache.commons.lang3.StringUtils
import org.springframework.http.HttpHeaders
import org.springframework.http.HttpStatus
import org.springframework.http.HttpStatusCode
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.MethodArgumentNotValidException
import org.springframework.web.bind.annotation.RestControllerAdvice
import org.springframework.web.context.request.WebRequest
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler

@RestControllerAdvice
class ApiExceptionHandler : ResponseEntityExceptionHandler() {

    //@ExceptionHandler(value = [CleanArchException::class])
    fun cleanArchException(exception: RuntimeException, request: WebRequest): ResponseEntity<Any> {
        //val httpStatus = (exception as CleanArchException).httpStatus
        val message = exception.localizedMessage

        //log.error { "[ApiExceptionHandler] Error message: $message" }
        return getObjectResponseEntity(exception, request, HttpStatus.BAD_REQUEST, message)
    }

    /*
        @ExceptionHandler(value = [FeignException::class])
        fun feignException(exception: FeignException, request: WebRequest): ResponseEntity<Any> {
            val message = exception.localizedMessage

            log.error { "[feignException] Error message: $message" }
            return getObjectResponseEntity(exception, request, HttpStatus.valueOf(exception.status()), message)
        }
    */

    override fun handleMethodArgumentNotValid(
        ex: MethodArgumentNotValidException,
        headers: HttpHeaders,
        status: HttpStatusCode,
        request: WebRequest,
    ): ResponseEntity<Any>? {
        val message = ex.bindingResult.fieldError?.let {
            StringUtils.joinWith(" ", "field", it.field, it.defaultMessage)
        }.orEmpty()

        return getObjectResponseEntity(ex, request, HttpStatus.BAD_REQUEST, message)
    }

    private fun getObjectResponseEntity(
        exception: Exception,
        request: WebRequest,
        httpStatus: HttpStatus,
        message: String,
    ): ResponseEntity<Any> {
        val response = ApiError(message)
        return createResponseEntity(response, HttpHeaders(), httpStatus, request) as ResponseEntity<Any>
    }
}
