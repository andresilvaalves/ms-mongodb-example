
create table student
(
    id         uuid    not null
        primary key,
    active     boolean not null,
    created_at timestamp(6),
    update_at  timestamp(6),
    birth_day  date,
    email      varchar(255),
    name       varchar(255)
);