#!/bin/sh
set -e

COMMAND_LINE="java -jar /app.jar -Dloader.main=$MAIN_CLASS org.springframework.boot.loader.PropertiesLauncher"

echo "";echo "Start command line: $COMMAND_LINE";echo ""

exec $COMMAND_LINE