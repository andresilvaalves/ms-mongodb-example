package io.andresilvaalves.mongodb.core.gateway

import io.andresilvaalves.mongodb.core.domain.StudentDomain
import org.springframework.data.domain.Page
import java.util.UUID

interface StudentGateway {

    suspend fun findById(uuid: UUID): StudentDomain
    suspend fun findAll(active: Boolean, page: Int, pageSize: Int): Page<StudentDomain>
    suspend fun save(domain: StudentDomain): StudentDomain
    suspend fun update(uuid: UUID, domain: StudentDomain): StudentDomain
    suspend fun delete(uuid: UUID)

    suspend fun findByName(name: String): List<StudentDomain>
}
