package io.andresilvaalves.mongodb.core.domain

import java.time.LocalDate
import java.util.UUID

data class StudentDomain(
    val id: UUID? = null,
    val name: String,
    val email: String,
    val birthDay: LocalDate? = null,
    val phoneNumberDomain: PhoneNumberDomain? = null,
)
