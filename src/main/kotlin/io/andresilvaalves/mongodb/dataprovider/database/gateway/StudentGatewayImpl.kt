package io.andresilvaalves.mongodb.dataprovider.database.gateway

import io.andresilvaalves.mongodb.core.domain.StudentDomain
import io.andresilvaalves.mongodb.core.gateway.StudentGateway
import io.andresilvaalves.mongodb.dataprovider.database.mapper.StudentModelMapper
import io.andresilvaalves.mongodb.dataprovider.database.repository.StudentRepository
import kotlinx.coroutines.runBlocking
import org.springframework.data.domain.Page
import org.springframework.data.domain.PageRequest
import org.springframework.stereotype.Service
import java.util.UUID

@Service
class StudentGatewayImpl(
    private val studentRepository: StudentRepository,
    private val studentModelMapper: StudentModelMapper,
) : StudentGateway {
    override suspend fun findById(uuid: UUID): StudentDomain {
        return runBlocking {
            studentRepository.findById(uuid).let { studentModelMapper.toDomain(it.get()) }
        }
    }

    override suspend fun findAll(active: Boolean, page: Int, pageSize: Int): Page<StudentDomain> {
        return runBlocking {
            studentRepository.findAll(PageRequest.of(page, pageSize)).map(studentModelMapper::toDomain)
        }
    }

    override suspend fun findByName(name: String): List<StudentDomain> {
        return runBlocking {
            studentRepository.findByNameLikeIgnoreCase(name).map { studentModelMapper.toDomain(it) }
        }
    }

    override suspend fun save(domain: StudentDomain): StudentDomain {
        return runBlocking {
            val newModel = studentModelMapper.toModel(domain.copy(id = UUID.randomUUID()))
            val saved = studentRepository.insert(newModel)
            saved.let(studentModelMapper::toDomain)
        }
    }

    override suspend fun update(uuid: UUID, domain: StudentDomain): StudentDomain {
        return runBlocking {
            val studentModel = studentRepository.findById(uuid).orElseThrow { RuntimeException() }
            studentModelMapper.update(studentModel, domain)
            studentRepository.save(studentModel).let(studentModelMapper::toDomain)
        }
    }

    override suspend fun delete(uuid: UUID) {
        runBlocking {
            studentRepository.deleteById(uuid)
        }
    }
}