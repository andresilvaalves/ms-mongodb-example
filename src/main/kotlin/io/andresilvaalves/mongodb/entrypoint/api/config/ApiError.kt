package io.andresilvaalves.mongodb.entrypoint.api.config

import java.time.LocalDateTime

data class ApiError(
    val message: String,
    private val timestamp: LocalDateTime,
) {

    constructor(message: String) : this(timestamp = LocalDateTime.now(), message = message)
}
