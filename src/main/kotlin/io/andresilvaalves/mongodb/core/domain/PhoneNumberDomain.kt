package io.andresilvaalves.mongodb.core.domain

data class PhoneNumberDomain(
    val codeArea: Int,
    val number: String,
)
