package io.andresilvaalves.mongodb.entrypoint.api.payload

import java.util.UUID

data class StudentRequest(
    val id: UUID,
    val name: String,
    val email: String,
)
