package io.andresilvaalves.mongodb

import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication

@SpringBootApplication
class MsMongodbExampleApplication

fun main(args: Array<String>) {
    runApplication<MsMongodbExampleApplication>(*args)
}
