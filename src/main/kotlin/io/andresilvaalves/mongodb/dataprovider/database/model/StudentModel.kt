package io.andresilvaalves.mongodb.dataprovider.database.model

import org.springframework.data.annotation.Id
import org.springframework.data.mongodb.core.mapping.Document
import org.springframework.data.mongodb.core.mapping.MongoId
import java.util.UUID

@Document
data class StudentModel(
    @Id
    val id: UUID,
    var name: String,
    var email: String? = null,
)
