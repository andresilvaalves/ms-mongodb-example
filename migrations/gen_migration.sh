TIMESTAMP=$(date +"%Y%m%d%H%M%S")

DIR="./poc_student"
FILENAME="V${TIMESTAMP}__$1.sql"

touch "$DIR/$FILENAME"
echo "Migration created: $DIR/$FILENAME"
